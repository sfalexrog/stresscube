//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_CUBE_H
#define VINOKUROVA_STRESSCUBE_CUBE_H

#include "Model.h"

Mesh createCubeMesh();
Model loadCube();

#endif //VINOKUROVA_STRESSCUBE_CUBE_H
