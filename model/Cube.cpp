//
// Created by sf on 12/30/16.
//

#include "Cube.h"
#include "util/Logger.h"

Mesh createCubeMesh()
{
    std::vector<glm::vec3> cubePoints{
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f,  1.0f, -1.0f},
            {-1.0f,  1.0f, -1.0f},
            {-1.0f, -1.0f,  1.0f},
            {1.0f, -1.0f,  1.0f},
            {1.0f,  1.0f,  1.0f},
            {-1.0f,  1.0f,  1.0f}
    };

    std::vector<glm::vec3> cubeColors{
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f},
            {0.0f,  0.0f,  0.0f}
    };

    std::vector<GLuint> cubeIndices{
            0, 1, 2,  // Задняя грань
            0, 3, 2,  //

            0, 7, 3,  // Левая грань
            0, 7, 4,  //

            7, 4, 5,  // Передняя грань
            7, 6, 5,  //

            1, 6, 5,  // Правая грань
            1, 6, 2,

            2, 3, 6,  // Верхняя грань
            7, 3, 6,

            0, 4, 5,  // Нижняя грань
            0, 1, 5
    };

    Log(LOG_DEBUG) << "Uploading cube to video memory";

    Mesh cubeMesh = {cubePoints, cubeColors, cubeIndices};
    return cubeMesh;
}

Model loadCube()
{
    Model cubeModel;
    cubeModel.addMesh(createCubeMesh());

    return cubeModel;
}

