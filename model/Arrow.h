//
// Created by sf on 12/31/16.
//

#ifndef VINOKUROVA_STRESSCUBE_ARROW_H
#define VINOKUROVA_STRESSCUBE_ARROW_H

#include "Model.h"

//Model createArrow(glm::vec3 position, glm::vec3 direction);
class Arrow: public Model
{
private:
    glm::vec3 position;
    glm::vec3 direction;
    GLfloat length;
    void updateTransforms();
public:
    constexpr static const float ARROW_HEAD_LENGTH = 0.12f;
    constexpr static const float ARROW_CONE_RADIUS = 0.07f;
    constexpr static const float ARROW_CYL_RADIUS = 0.03f;
    constexpr static const float DRAW_THRESHOLD = 0.01f;
    Arrow(glm::vec3 pos, glm::vec3 dir, GLfloat len);
    void setDirection(glm::vec3 dir);
    void setPosition(glm::vec3 pos);
    void setLength(GLfloat length);
    void draw(const Shader &s) override;
};


#endif //VINOKUROVA_STRESSCUBE_ARROW_H
