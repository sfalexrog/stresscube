//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_CONE_H
#define VINOKUROVA_STRESSCUBE_CONE_H

#include "Model.h"

Mesh createConeMesh(int numSides);
Model loadCone(int numSides);

#endif //VINOKUROVA_STRESSCUBE_CONE_H
