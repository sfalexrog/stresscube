//
// Created by sf on 12/30/16.
//

#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "Cylinder.h"

Mesh createCylinderMesh(int numSides)
{
    std::vector<glm::vec3> cylPos;
    std::vector<glm::vec3> cylCol;
    std::vector<GLuint> cylInd;

    // Close centerpoint
    cylPos.push_back(glm::vec3{0.0f, 0.0f, 0.0f});
    cylCol.push_back(glm::vec3{1.0f, 1.0f, 1.0f});

    // Sides
    for(int i = 0; i < numSides; ++i)
    {
        GLfloat angle = i * 2 * M_PI / numSides;
        glm::vec3 closePoint{1.0f * cos(angle), 1.0f * sin(angle), 0.0f};
        glm::vec3 farPoint{1.0f * cos(angle), 1.0f * sin(angle), 1.0f};
        cylPos.push_back(closePoint);
        cylPos.push_back(farPoint);
        cylCol.push_back(glm::vec3{1.0f, 0.0f, 0.0f});
        cylCol.push_back(glm::vec3{1.0f, 0.0f, 0.0f});
    }

    // Far centerpoint
    cylPos.push_back(glm::vec3{0.0f, 0.0f, 1.0f});
    cylCol.push_back(glm::vec3{0.0f, 0.0f, 0.0f});
    // Close side
    for(unsigned int i = 0; i < numSides; ++i)
    {
        cylInd.push_back(0);
        cylInd.push_back(1 + (2 * i) % (2 * numSides));
        cylInd.push_back(1 + (2 * i + 2) % (2 * numSides));

    }

    // Far side
    for(unsigned int i = 0; i < numSides; ++i)
    {
        cylInd.push_back(cylPos.size() - 1);
        cylInd.push_back(2 + (2 * i) % (2 * numSides));
        cylInd.push_back(2 + (2 * i + 2) % (2 * numSides));
    }

    // Side(???) side
    for(unsigned int i = 0; i < numSides; ++i)
    {
        cylInd.push_back(1 + (2 * i) % (2 * numSides));
        cylInd.push_back(1 + (2 * i + 2) % (2 * numSides));
        cylInd.push_back(2 + (2 * i) % (2 * numSides));
        cylInd.push_back(2 + (2 * i) % (2 * numSides));
        cylInd.push_back(2 + (2 * i + 2) % (2 * numSides));
        cylInd.push_back(1 + (2 * i + 2) % (2 * numSides));
    }

    Mesh cylinderMesh = {cylPos, cylCol, cylInd};
    return cylinderMesh;
}

Model loadCylinder(int numSides)
{
    Model cylinder;
    cylinder.addMesh(createCylinderMesh(numSides));

    return cylinder;
}
