//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_MODEL_H
#define VINOKUROVA_STRESSCUBE_MODEL_H

#include <vector>
#include <glm/common.hpp>
#include <glm/matrix.hpp>

#include "glload/gl_glcore_3_3.h"

class Shader;

struct Mesh
{
    std::vector<glm::vec3> pos;
    std::vector<glm::vec3> col;
    std::vector<GLuint> ind;
};

class Model
{
private:
    struct ModelPart {
        Mesh mesh;
        GLuint posBuffer;
        GLuint colBuffer;
        GLuint indBuffer;
        glm::mat4 transform;
    };

    std::vector<ModelPart> meshes;
    glm::mat4 modelTransform;

public:
    virtual void draw(const Shader &s);
    void transform(const glm::mat4 &t);
    void addMesh(const Mesh& mesh, const glm::mat4 meshTransform = glm::mat4(1.0f));
    void transformMesh(const size_t meshIdx, const glm::mat4 meshTransform);

};

#endif //VINOKUROVA_STRESSCUBE_MODEL_H
