//
// Created by sf on 1/3/17.
//

#ifndef VINOKUROVA_STRESSCUBE_STRESSCUBE_H
#define VINOKUROVA_STRESSCUBE_STRESSCUBE_H

#include <vector>
#include "Model.h"
#include "Cube.h"
#include "Arrow.h"
#include "CubeOutline.h"

class StressCube
{
private:
    Model cube;
    Model outline;
    std::vector<Arrow> vectors;
    std::vector<std::vector<double>> stresses;
    double maxNegStress;
    double maxPosStress;
public:
    StressCube() : cube(loadCube()), outline(cubeOutline()), maxNegStress(-0.0), maxPosStress(0.0)
    {
        // X axis
        vectors.push_back(Arrow(glm::vec3{1.0f, 0.0f, 0.0f}, glm::vec3{1.0f, 0.0f, 0.0f}, 1.0f)); // 0
        // Y axis
        vectors.push_back(Arrow(glm::vec3{0.0f, 1.0f, 0.0f}, glm::vec3{0.0f, 1.0f, 0.0f}, 1.0f)); // 1
        // Z axis
        vectors.push_back(Arrow(glm::vec3{0.0f, 0.0f, 1.0f}, glm::vec3{0.0f, 0.0f, 1.0f}, 1.0f)); // 2

        // XY plane
        vectors.push_back(Arrow(glm::vec3{0.0f, -0.5f, 1.0f}, glm::vec3{0.0f, 1.0f, 0.0f}, 1.0f)); // 3 - tauXY
        vectors.push_back(Arrow(glm::vec3{-0.5f, 0.0f, 1.0f}, glm::vec3{1.0f, 0.0f, 0.0f}, 1.0f)); // 4 - tauYX
        // YZ plane
        vectors.push_back(Arrow(glm::vec3{1.0f, -0.5f, 0.0f}, glm::vec3{0.0f, 1.0f, 0.0f}, 1.0f)); // 5 - tauZY
        vectors.push_back(Arrow(glm::vec3{1.0f, 0.0f, -0.5f}, glm::vec3{0.0f, 0.0f, 1.0f}, 1.0f)); // 6 - tauYZ
        // XZ plane
        vectors.push_back(Arrow(glm::vec3{-0.5f, 1.0f, 0.0f}, glm::vec3{1.0f, 0.0f, 0.0f}, 1.0f)); // 7 - tauXZ
        vectors.push_back(Arrow(glm::vec3{0.0f, 1.0f, -0.5f}, glm::vec3{0.0f, 0.0f, 1.0f}, 1.0f)); // 8 - tauZX

        stresses.resize(3);
        for(int i = 0; i < 3; ++i)
        {
            stresses[i].resize(3);
            for(int j = 0; j < 3; ++j)
            {
                stresses[i][j] = 0;
            }
        }
    }

    void setStressComponent(int i, int j, double value)
    {
        stresses[i][j] = value;
        stresses[j][i] = value;
        maxPosStress = 0.0;
        maxNegStress = -0.0;
        for(int a = 0; a < 3; ++a)
        {
            for(int b = 0; b < 3; ++b)
            {
                maxPosStress = std::max(maxPosStress, stresses[a][b]);
                maxNegStress = std::min(maxNegStress, stresses[a][b]);
            }
        }
        double scaleFactor = std::max(maxPosStress, -maxNegStress);
        if (scaleFactor > 0.000001)
        {
            GLfloat sigmaX = 1.2f * stresses[0][0] / scaleFactor;
            GLfloat sigmaY = 1.2f * stresses[1][1] / scaleFactor;
            GLfloat sigmaZ = 1.2f * stresses[2][2] / scaleFactor;

            GLfloat tauXY = 1.5f * stresses[0][1] / scaleFactor;
            GLfloat tauXZ = 1.5f * stresses[2][0] / scaleFactor;
            GLfloat tauYZ = 1.5f * stresses[1][2] / scaleFactor;
            // sigmaX
            if (sigmaX > 0)
            {
                vectors[0].setDirection(glm::vec3{1, 0, 0});
                vectors[0].setPosition(glm::vec3{1, 0, 0});
                vectors[0].setLength(sigmaX);
            }
            else
            {
                vectors[0].setDirection(glm::vec3{-1, 0, 0});
                vectors[0].setPosition(glm::vec3{1 - sigmaX + Arrow::ARROW_HEAD_LENGTH + Arrow::ARROW_CYL_RADIUS, 0, 0});
                vectors[0].setLength(-sigmaX);
            }

            // sigmaY
            if (sigmaY > 0)
            {
                vectors[1].setDirection(glm::vec3{0, 1, 0});
                vectors[1].setPosition(glm::vec3{0, 1, 0});
                vectors[1].setLength(sigmaY);
            }
            else
            {
                vectors[1].setDirection(glm::vec3{0, -1, 0});
                vectors[1].setPosition(glm::vec3{0, 1 - sigmaY + Arrow::ARROW_HEAD_LENGTH + Arrow::ARROW_CYL_RADIUS, 0});
                vectors[1].setLength(-sigmaY);
            }

            // sigmaZ
            if (sigmaZ > 0)
            {
                vectors[2].setDirection(glm::vec3{0, 0, 1});
                vectors[2].setPosition(glm::vec3{0, 0, 1});
                vectors[2].setLength(sigmaZ);
            }
            else
            {
                vectors[2].setDirection(glm::vec3{0, 0, -1});
                vectors[2].setPosition(glm::vec3{0, 0, 1 - sigmaZ + Arrow::ARROW_HEAD_LENGTH + Arrow::ARROW_CYL_RADIUS});
                vectors[2].setLength(-sigmaZ);
            }

            // tauXY
            vectors[3].setLength(tauXY);
            vectors[3].setPosition(glm::vec3{1.0f, -tauXY/2.0f, 0.0f});
            vectors[4].setLength(tauXY);
            vectors[4].setPosition(glm::vec3{-tauXY/2.0f, 1.0f, 0.0f});

            // tauYZ
            vectors[5].setLength(tauYZ);
            vectors[5].setPosition(glm::vec3{0.0f, -tauYZ/2.0f, 1.0f});
            vectors[6].setLength(tauYZ);
            vectors[6].setPosition(glm::vec3{0.0f, 1.0f, -tauYZ/2.0f});

            // tauXZ
            // Workaround for arrows getting all over the place - there's probably a
            // bug somewhere deep, but I don't care about it enough.
            if (tauXZ > 0)
            {
                vectors[7].setDirection(glm::vec3{1.0f, 0.0f, 0.0f});
                vectors[8].setDirection(glm::vec3{0.0f, 0.0f, 1.0f});
                vectors[7].setLength(tauXZ);
                vectors[7].setPosition(glm::vec3{-tauXZ/2.0f, 0.0f, 1.0f});
                vectors[8].setLength(tauXZ);
                vectors[8].setPosition(glm::vec3{1.0f, 0.0f, -tauXZ/2.0f});
            }
            else
            {
                vectors[7].setDirection(glm::vec3{-1.0f, 0.0f, 0.0f});
                vectors[8].setDirection(glm::vec3{0.0f, 0.0f, -1.0f});
                vectors[7].setLength(-tauXZ);
                vectors[7].setPosition(glm::vec3{-tauXZ/2.0f, 0.0f, 1.0f});
                vectors[8].setLength(-tauXZ);
                vectors[8].setPosition(glm::vec3{1.0f, 0.0f, -tauXZ/2.0f});
            }
        }
    }

    void draw(const Shader &s)
    {
        cube.draw(s);
        outline.draw(s);
        for(auto &v: vectors)
        {
            v.draw(s);
        }
    }

    void transform(glm::mat4 trfmMatrix)
    {
        cube.transform(trfmMatrix);
        outline.transform(trfmMatrix);
        for(auto &v: vectors)
        {
            v.transform(trfmMatrix);
        }
    }

};

#endif //VINOKUROVA_STRESSCUBE_STRESSCUBE_H
