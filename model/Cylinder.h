//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_CYLINDER_H
#define VINOKUROVA_STRESSCUBE_CYLINDER_H

#include "Model.h"

Mesh createCylinderMesh(int numSides);
Model loadCylinder(int numSides);

#endif //VINOKUROVA_STRESSCUBE_CYLINDER_H
