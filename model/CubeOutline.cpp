//
// Created by sf on 12/31/16.
//

#include <glm/gtc/matrix_transform.hpp>
#include "CubeOutline.h"
#include "Cube.h"

const GLfloat EPS = 0.005f;

Model cubeOutline()
{
    Mesh outlineBlock = createCubeMesh();
    for(auto &col: outlineBlock.col)
    {
        col = glm::vec3{1.0f, 1.0f, 1.0f};
    }
    Model result;
    glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3{EPS, EPS, 1.0f +  EPS});
    glm::mat4 translate = glm::translate(glm::mat4(1.0f), glm::vec3{1.0f, 1.0f, 0.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{1.0f, -1.0f, 0.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{-1.0f, -1.0f, 0.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{-1.0f, 1.0f, 0.0f});
    result.addMesh(outlineBlock, translate * scale);

    scale = glm::scale(glm::mat4(1.0f), glm::vec3{EPS, 1.0f + EPS, EPS});
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{1.0f, 0.0f, 1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{-1.0f, 0.0f, 1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{1.0f, 0.0f, -1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{-1.0f, 0.0f, -1.0f});
    result.addMesh(outlineBlock, translate * scale);

    scale = glm::scale(glm::mat4(1.0f), glm::vec3{1.0f + EPS, EPS, EPS});
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{0.0f, 1.0f, 1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{0.0f, -1.0f, 1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{0.0f, 1.0f, -1.0f});
    result.addMesh(outlineBlock, translate * scale);
    translate = glm::translate(glm::mat4(1.0f), glm::vec3{0.0f, -1.0f, -1.0f});
    result.addMesh(outlineBlock, translate * scale);

    return result;
}
