//
// Created by sf on 12/30/16.
//

#include "Cone.h"

Mesh createConeMesh(int numSides)
{
    std::vector<glm::vec3> conePos;
    std::vector<glm::vec3> coneCol;
    std::vector<GLuint> coneInd;

    conePos.push_back(glm::vec3{0.0f, 0.0f, 0.0f});
    coneCol.push_back(glm::vec3{1.0f, 1.0f, 1.0f});

    // Sides
    for(int i = 0; i < numSides; ++i)
    {
        GLfloat angle = i * 2 * M_PI / numSides;
        glm::vec3 closePoint{1.0f * cos(angle), 1.0f * sin(angle), 0.0f};
        conePos.push_back(closePoint);
        coneCol.push_back(glm::vec3{1.0f, 0.0f, 0.0f});
    }

    // Far centerpoint
    conePos.push_back(glm::vec3{0.0f, 0.0f, 1.0f});
    coneCol.push_back(glm::vec3{1.0f, 1.0f, 1.0f});
    // Close side
    for(unsigned int i = 0; i < numSides; ++i)
    {
        coneInd.push_back(0);
        coneInd.push_back(1 + (i) % (numSides));
        coneInd.push_back(1 + (i + 1) % (numSides));
    }

    // Far side
    for(unsigned int i = 0; i < numSides; ++i)
    {
        coneInd.push_back(conePos.size() - 1);
        coneInd.push_back(1 + (i) % (numSides));
        coneInd.push_back(1 + (i + 1) % (numSides));
    }

    Mesh coneMesh = {conePos, coneCol, coneInd};
    return coneMesh;
}

Model loadCone(int numSides)
{
    Model cone;
    cone.addMesh(createConeMesh(numSides));
    return cone;
}
