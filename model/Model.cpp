//
// Created by sf on 12/30/16.
//

#include <glm/gtc/type_ptr.hpp>
#include "Model.h"
#include "shader/Shader.h"

void Model::draw(const Shader &s)
{
    for(const auto& mp: meshes)
    {
        glm::mat4 absTransform = modelTransform * mp.transform;
        glUniformMatrix4fv(s.getUnif("transform"), 1, GL_FALSE, glm::value_ptr(absTransform));
        glBindBuffer(GL_ARRAY_BUFFER, mp.posBuffer);
        auto attrPos = s.getAttr("pos");
        glEnableVertexAttribArray(attrPos);
        glVertexAttribPointer(attrPos, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, mp.colBuffer);
        attrPos = s.getAttr("v_color");
        glEnableVertexAttribArray(attrPos);
        glVertexAttribPointer(attrPos, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mp.indBuffer);
        glDrawElements(GL_TRIANGLES, mp.mesh.ind.size(), GL_UNSIGNED_INT, 0);
    }
}

void Model::transform(const glm::mat4 &t)
{
    modelTransform = t;
}

void Model::addMesh(const Mesh &mesh, const glm::mat4 meshTransform)
{
    ModelPart mp;
    mp.mesh = mesh;
    mp.transform = meshTransform;
    glGenBuffers(1, &(mp.posBuffer));
    glGenBuffers(1, &(mp.colBuffer));
    glGenBuffers(1, &(mp.indBuffer));

    glBindBuffer(GL_ARRAY_BUFFER, mp.posBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * mp.mesh.pos.size(), &mp.mesh.pos[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, mp.colBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * mp.mesh.col.size(), &mp.mesh.col[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mp.indBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mp.mesh.ind.size(), &mp.mesh.ind[0], GL_STATIC_DRAW);

    meshes.push_back(mp);
}

#define STR(x) #x

void Model::transformMesh(const size_t meshIdx, const glm::mat4 meshTransform)
{
    if (meshIdx >= meshes.size())
    {
        Log(LOG_ERROR) << "Called " STR(__func__) " with meshIdx > meshes.size()";
    }
    meshes[meshIdx].transform = meshTransform;
}
