//
// Created by sf on 12/31/16.
//

#include "Arrow.h"
#include "Cone.h"
#include "Cylinder.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

void Arrow::updateTransforms()
{
    glm::vec3 nv = glm::cross(glm::vec3{0, 0, 1}, direction);
    if (glm::length(nv) < 0.001f)
    {
        nv = glm::cross(glm::vec3{0, 1, 0}, direction);
    }
    glm::vec3 norm = glm::normalize(nv);
    auto cosAngle = glm::dot(glm::vec3{0.0f, 0.0f, 1.0f}, direction) / (glm::length(glm::vec3{0, 0, 1}) * glm::length(direction));
    GLfloat angle = acos(cosAngle);

    glm::mat4 cylScale = glm::scale(glm::mat4(1.0f), glm::vec3{ARROW_CYL_RADIUS, ARROW_CYL_RADIUS, length});
    glm::mat4 cylRotate = glm::rotate(glm::mat4(1.0f), angle, norm);
    glm::mat4 cylTranslate = glm::translate(glm::mat4(1.0f), position);

    glm::mat4 coneScale = glm::scale(glm::mat4(1.0f), glm::vec3{ARROW_CONE_RADIUS, ARROW_CONE_RADIUS, ARROW_HEAD_LENGTH});
    glm::mat4 coneRotate = glm::rotate(glm::mat4(1.0f), angle, norm);
    glm::mat4 coneTranslate = glm::translate(glm::mat4(1.0f), position + glm::normalize(direction) * length);

    transformMesh(0, cylTranslate * cylRotate * cylScale);
    transformMesh(1, coneTranslate * coneRotate * coneScale);
}

Arrow::Arrow(glm::vec3 pos, glm::vec3 dir, GLfloat len) : Model(), position(pos), direction(dir), length(len)
{
    addMesh(createCylinderMesh(30));
    addMesh(createConeMesh(30));
    updateTransforms();
}

void Arrow::setLength(GLfloat len)
{
    if (len < 0)
    {
        direction *= -1;
        len *= -1;
    }
    length = len;
    updateTransforms();
}

void Arrow::setDirection(glm::vec3 dir)
{
    direction = dir;
    updateTransforms();
}

void Arrow::setPosition(glm::vec3 pos)
{
    position = pos;
    updateTransforms();
}

void Arrow::draw(const Shader &s)
{
    if (std::abs(length) > DRAW_THRESHOLD)
    {
        Model::draw(s);
    }
}
