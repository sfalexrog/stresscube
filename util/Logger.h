//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_LOGGER_H
#define VINOKUROVA_STRESSCUBE_LOGGER_H

#include <iostream>
#include <sstream>

namespace _Logger {
    enum Severity {
        LOG_DEBUG = 0,
        LOG_INFO,
        LOG_WARN,
        LOG_ERROR,
        LOG_FATAL
    };

    class LogWriter
    {
    private:
        std::stringstream log_buf;
        Severity severity;

        std::string sevStr()
        {
            switch(severity){
                case LOG_DEBUG:
                    return "[DEBUG] ";
                case LOG_INFO:
                    return "[INFO] ";
                case LOG_WARN:
                    return "[WARN] ";
                case LOG_ERROR:
                    return "[ERROR] ";
                case LOG_FATAL:
                    return "[FATAL] ";
            }
        }

        static std::ostream& getStream(Severity s)
        {
            if (s < LOG_ERROR)
            {
                return std::cout;
            }
            else
            {
                return std::cerr;
            }
        }

    public:

        static Severity& minSeverity()
        {
            static Severity minSeverity = LOG_DEBUG;
            return minSeverity;
        }

        std::stringstream& log()
        {
            return log_buf;
        }

        LogWriter(Severity s): severity(s) {}
        ~LogWriter()
        {
            if (severity >= minSeverity())
            {
                getStream(severity) << sevStr() << log_buf.str() << std::endl;
            }
        }
    };
};



#define Log(LOGLEVEL) _Logger::LogWriter(_Logger::LOGLEVEL).log()

#endif //VINOKUROVA_STRESSCUBE_LOGGER_H
