// ��������� ���� � ������� OpenGL
//
// ������������ �����/����������, ��� ������� �������:
// ������������ ���� iostream ����� ��� ������ ��������������� ��������� � ���� ��������� ������
#include <iostream>
// ���������� SDL2 ������������ ��� �������� ���� � �������� ��������� OpenGL ��� �������� � ����������
// ���������
#include <SDL2/SDL.h>
// ��������� glload ������������ ������������� ��� ������ 3.3 OpenGL. �� ����� ��� �������� ������� OpenGL.
#include "glload/gl_glcore_3_3.h"
// ������������ ���� � ��������������� ���������
#include <cmath>
// ��� ������ � ���������, ������������� � OpenGL, �� ���������� ���������� glm (OpenGL Mathematics).
// ��� ���������������� � ���� ������ ������������ ������, � �������� ���������� ������ �����������
// ��� ���������� � �����.
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

#include "util/Logger.h"
#include "imgui/imgui.h"
#include "imgui/sdl2impl/imgui_impl_sdl_gl3.h"
#include "shader/Shader.h"

#include "model/StressCube.h"

using namespace std;

// � ����������� OpenGL ���������, ��� � ���������� "�� ��������" ������� �������������� � �����������
// ���������� �� ������. ��� ������� ����� ����������� � ������� ��������� ����������� �������� ���
// ���������� - "��������".

// ��� ������ ������ �� ���������� ���� ��������� ������ � ���� ����������� (����������) ������.
const char VTX_SHADER[] =
"#version 330\n"      // ��� ������ ��������� ������ ����� �������� GLSL
"\n"
"in vec3 pos;\n"      // ������� ������ ��� �������: ������� �������
"in vec3 v_color;\n"  // � � ����
"\n"
"uniform mat4 proj;\n"       // ��������, ���������� ��� ������� ������� ���������:
"uniform mat4 transform;\n"  // ������� �������� � �������������� ������
"\n"
"out vec3 f_color;\n" // �������� ������ ��� �������: ���� �������
"\n"                  // (�������� ������ ����������� ������������ �������)
"void main() {\n"
"    gl_Position = proj * transform * vec4(pos, 1.0);\n" // ��������� ������� �������...
"    f_color = v_color;\n" // ...� ������� ���� ������� � ����������� ������
"    \n"
"}\n";

// ��� ������������ (�����������) ������� ����� ����������� ��� ������ �����
// ��������� ���������, �������� �� �����. ��������, ����������� �� ����������
// �������, ����� ������� ��������������� ��� ������� �������.
const char FRG_SHADER[] =
"#version 330\n"
"\n"
"precision mediump float;\n" // ���������, ��� �� ��������� ��� ����� � ���������
"\n"                         // ������ ����� ����� ������� �������� (~16 ��� �� ��������)
"in vec3 f_color;\n"         // ������� �������� �� ���������� �������
"out vec4 color;\n"          // ��������� ������ ������������ ������� - ���� �������.
"void main() {\n"
"    color = vec4(f_color, 1.0);\n" // ������ ���������� ���� ������� "��� ����"
"}\n";

// ����� ����� ������� ���� ������� "���������" - �� ����������� ��� ��������� �������
// ����� ���� ������ "������������" � ����� Shader, ����� - � ����� Model
struct GLState
{
    GLuint vao;    // ������� ������ ��������� �������� (vertex array object)
    glm::mat4 projM;   // ������� �������� � �������������� ������ � ���� ����� ���������� glm,
    glm::mat4 trfmM;   // ������� ����������� ���������� � ����� GLSL (����� �������� OpenGL)
} glState;

// �������� ������� ��������
// ������� ������������� ����� �������� �� ����������� ������ ����, ���� ��������������
// ��������� ���������.
void initProj(int width, int height)
{
    // ����������� ����������� ������ ����
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    // � ���������, ��� �� ����� ������������� ������������� � ����� ������
    // 45 �������� �� �������, ���������� �������� ��������� �� ���������� 0.1 �������,
    // ���������� �������� ��������� 100 ������, � ����� ����� ���� �������� ���������:
    // �� ��������� � ����� (2, 2, 2), ������� �� ������ ��������� (����� 0, 0, 0),
    // � ������ (0, 1, 0) ���������, ��� ��� "����".
    glState.projM = //glm::perspective(45.0f, aspect, 0.1f, 100.0f) *
                    glm::ortho(-2.0f * aspect, 2.0f * aspect, -2.0f, 2.0f, 0.1f, 100.0f) *
                    glm::lookAt(glm::vec3{2.0f, 2.0f, 2.0f}, glm::vec3{0, 0, 0}, glm::vec3{0, 1, 0});
}

// ������� ���� ������ �������� ��� �� ���� angle
void rotateCube(double angleX, double angleY)
{
    // ���������, ��� ����� ��������� ��� �� angle �������� ������
    // ���, ���������� �������� (0, 1, 0)
    auto rotX = glm::rotate(glm::mat4(1.0f), (GLfloat)angleY, glm::vec3{1, 0, -1});
    auto rotY = glm::rotate(glm::mat4(1.0f), (GLfloat)angleX, glm::vec3{0, 1, 0});
    glState.trfmM = rotX * rotY;
}

// ������ �������� OpenGL ��� �������� ����
SDL_GLContext initGL(SDL_Window *window)
{
    // ��������� �������� ��������� ���������:
    //   �������� ������ ���� core (��� ��������� ���������� �������)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    //   ������ ��������� (��� � OpenGL) - 3.3 (� ��� ��������� ������ �������� ����� ����������)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    //   ����������� ���� ��� ��������� � 8 ������ �� ������ ����� (������ � �������������)
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    //   ����������� ����� ������� � �� ����� ��� 16 ������
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    // �������� ������� ��������. ����������� ��� �����, ��������, ���� �������� ��� ������ �� ������������ ������ 3.3 OpenGL
    SDL_GLContext context = SDL_GL_CreateContext(window);
    if (!context)
    {
        // �������� ������������ � �������, ��������� ����� ��������� �������� ������
        Log(LOG_ERROR) << "Could not create OpenGL context! SDL reports error: " << SDL_GetError();
        return nullptr;
    }

    // ��������� ���������� ������������
    Log(LOG_INFO) << "Created a new OpenGL context for our application";
    Log(LOG_INFO) << "Requested configuration: 3.3 core, R8G8B8A8, depth bits: 16";
    int major, minor, profile;
    int r, g, b, a;
    int depth;
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &profile);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &major);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minor);
    SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &r);
    SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &g);
    SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &b);
    SDL_GL_GetAttribute(SDL_GL_ALPHA_SIZE, &a);
    SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depth);

    Log(LOG_INFO) << "Received configuration: " << major << "." << minor << " "
                  << (profile == SDL_GL_CONTEXT_PROFILE_CORE ? "core" : "compatibility")
                  << ", R" << r << "G" << g << "B" << b << "A" << a
                  << ", depth bits: " << depth;

    // ���������, ��� ������������ ������ ������������ ������ ���� ��������� ���������
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    // �������� �������� ������� - ��� ����� ��� ����, ����� ������ ������������ �� �����������
    // ��������.
    glEnable(GL_DEPTH_TEST);

    return context;
}

// ������� main ����� ������������� � SDL_main, � ������ ��������� ��� ��� ��� ������������.
#ifdef __cplusplus
extern "C" {
#endif

int main(int argc, char** argv)
{
    // ��������� ������� ����
    // TODO: ������� ���� ��������������.
    int WIN_WIDTH = 1024;
    int WIN_HEIGHT = 600;

    // �������������� �� ����� ���������� SDL, ������� �������� �� �������
    SDL_Init(SDL_INIT_VIDEO);
    // ������ ���� ����� ���������
    SDL_Window *w = SDL_CreateWindow("Stress Cube",
                                     SDL_WINDOWPOS_UNDEFINED,
                                     SDL_WINDOWPOS_UNDEFINED,
                                     WIN_WIDTH,
                                     WIN_HEIGHT,
                                     SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    // ������ ������� �� ��� ������� ���� ������� �������; ������� ������� �������, ��� ����������
    SDL_GetWindowSize(w, &WIN_WIDTH, &WIN_HEIGHT);
    // �������� ������� �������� OpenGL - ���� ��� ��� �� �������, ���������� ������
    // ������� OpenGL �������� � ����� ���������.
    SDL_GLContext context = initGL(w);

    // ���������� ��� ����������������� ���������� ���������� ImGui
    ImGui_ImplSdlGL3_Init(w);

    // ��������� �������� ��������� �� ������� ��������� ��������� SDL
    SDL_Event e;

    // ���� ���������� ���������
    bool quit = false;

    // ������� ���� - ����� ����� ���������� � 0
    int brightness = 128;

    glGenVertexArrays(1, &glState.vao);
    glBindVertexArray(glState.vao);

    // ������ ��������� ��������� � ��������� � � �������������.
    Shader shader{VTX_SHADER, FRG_SHADER};
    shader.use();

    StressCube sc;
    // ������ ������� �������� ��� ������ ����
    initProj(WIN_WIDTH, WIN_HEIGHT);
    // ��������� ���� �������� ������ - 0
    GLfloat angleX = 0.0, angleY = 0.0;
    // ���������� ���������� ������� ������� SDL ��� �������� ����
    auto prevTicks = SDL_GetTicks();

    bool dragging = false;
    int prevX, prevY;
    prevTicks = SDL_GetTicks();
    bool mouseInWindow = false;

    float sigmaX = 1.0f, sigmaY = 1.0f, sigmaZ = 1.0f;
    float tauXY = 1.0f, tauXZ = 1.0f, tauYZ = 1.0f;

    while (!quit)
    {
        int deltaX = 0, deltaY = 0;
        int dragStartX, dragStartY;
        // ������ � ��������� �� �������, ������� ��� ��������.
        while(SDL_PollEvent(&e))
        {
            ImGui_ImplSdlGL3_ProcessEvent(&e);
            switch(e.type)
            {
                case SDL_MOUSEBUTTONDOWN:
                    if (!dragging)
                    {
                        dragging = true;
                        //SDL_SetRelativeMouseMode(SDL_TRUE);
                        dragStartX = e.button.x;
                        dragStartY = e.button.y;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    dragging = false;
                    //SDL_SetRelativeMouseMode(SDL_FALSE);
                    break;
                case SDL_MOUSEMOTION:
                    deltaX = prevX - e.motion.x;
                    deltaY = prevY - e.motion.y;
                    prevX = e.motion.x;
                    prevY = e.motion.y;
                    break;

                // ���� ������ ��������� � �������� ���� ��������� - �������.
                case SDL_QUIT:
                    quit = true;
                    break;
            }
        }
        SDL_GetWindowSize(w, &WIN_WIDTH, &WIN_HEIGHT);
        initProj(WIN_WIDTH, WIN_HEIGHT);
        glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
        ImGui_ImplSdlGL3_NewFrame(w);
        mouseInWindow = ImGui::IsMouseHoveringAnyWindow();
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::Begin("Dragging Info");
            ImGui::Text("dragging: %s", dragging ? "true" : "false");
            ImGui::Text("drag start x: %d", dragStartX);
            ImGui::Text("drag start y: %d", dragStartY);
        if (dragging && !mouseInWindow)
        {
            angleX -= deltaX * 0.005;
            angleY -= deltaY * 0.005;
        }
        ImGui::End();

        ImGui::Begin("Angles");
        {
            ImGui::SliderFloat("X angle", &angleX, -M_PI, M_PI);
            ImGui::SliderFloat("Y angle", &angleY, -M_PI, M_PI);
        }
        ImGui::End();

        ImGui::Begin("Stresses");
        {
            ImGui::Text("Normal stresses");
            ImGui::SliderFloat("SigmaX", &sigmaX, -5, 5);
            ImGui::SliderFloat("SigmaY", &sigmaY, -5, 5);
            ImGui::SliderFloat("SigmaZ", &sigmaZ, -5, 5);

            ImGui::Text("Tangential stresses");
            ImGui::SliderFloat("TauXY", &tauXY, -5, 5);
            ImGui::SliderFloat("TauXZ", &tauXZ, -5, 5);
            ImGui::SliderFloat("TauYZ", &tauYZ, -5, 5);
        }
        ImGui::End();

        sc.setStressComponent(0, 0, sigmaX);
        sc.setStressComponent(1, 1, sigmaY);
        sc.setStressComponent(2, 2, sigmaZ);

        sc.setStressComponent(0, 1, tauXY);
        sc.setStressComponent(0, 2, tauXZ);
        sc.setStressComponent(1, 2, tauYZ);

        // ����������� ������� �������� �������
        GLfloat bf = brightness / 255.0;

        // ������� ����� ������, �������� ��� �������� ������
        glClearColor(bf, bf, bf, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // ������������ ����� �� �������� ���� � ����������� ���������
        // ���� ��������
        rotateCube(angleX, angleY);
        if (abs(angleX) >= M_PI) {angleX -= angleX/abs(angleX) * 2 * M_PI;}
        if (abs(angleY) >= M_PI) {angleY -= angleY/abs(angleY) * 2 * M_PI;}

        prevTicks = SDL_GetTicks();

        // ������ ��� ������. ���������� ������� �������� � ������ � ��� ������ ������ ��������� �����
        // ��������������, ����� ���� ��������� ��������� � ��������� ��������.
        glUniformMatrix4fv(shader.getUnif("proj"), 1, GL_FALSE, glm::value_ptr(glState.projM));

        sc.transform(glState.trfmM);
        sc.draw(shader);
        ImGui::Render();
        // ������ ������� ����� ������ � ����� �����������
        // (����������, ����������, ��� �� ����������)
        SDL_GL_SwapWindow(w);
    }
    // ��������� �� �����: ������� �������� OpenGL � ���� ����.
    ImGui_ImplSdlGL3_Shutdown();
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(w);
    return 0;
}
#ifdef __cplusplus
}
#endif // __cplusplus
