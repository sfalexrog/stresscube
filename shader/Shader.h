//
// Created by sf on 12/30/16.
//

#ifndef VINOKUROVA_STRESSCUBE_SHADER_H
#define VINOKUROVA_STRESSCUBE_SHADER_H

#include <map>
#include <unordered_map>
#include <bits/unordered_map.h>
#include "util/Logger.h"
#include "glload/gl_glcore_3_3.h"
#include <vector>

class Shader {
private:
    // Функция компиляции шейдера. В OpenGL шейдеры компилируются во время работы программы
    // (как правило, во время запуска/загрузки).
    static GLuint compileShader(const char* source, GLenum type)
    {
        // Говорим OpenGL, что будем создавать новый шейдер. Тип шейдера также надо указать
        // (вершинный/фрагментный/геометрический/...)
        GLuint shader = glCreateShader(type);
        // Загружаем исходный код шейдера...
        glShaderSource(shader, 1, &source, nullptr);
        // ...и пытаемся его скомпилировать.
        glCompileShader(shader);
        // Результат компиляции - работающий шейдер или какая-то ошибка. Узнаём это.
        GLint compileStatus;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
        // Если шейдер скомпилировался нормально - замечательно, передаём его номер вызвавшей функции
        if (compileStatus == GL_TRUE)
        {
            return shader;
        }
        // Что-то пошло не так - надо выяснить, в чём проблема, и сообщить об этом пользователю.
        // Сначала узнаём, насколько много символов содержится в сообщении об ошибке
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        // Выделяем память для хранения сообщения и копируем это сообщение туда
        char *log = new char[logLength];
        glGetShaderInfoLog(shader, logLength, nullptr, log);
        // Выводим сообщение об ошибке и указываем на невозможность создать шейдер.
        Log(LOG_ERROR) << "Could not compile shader: " << log;
        delete log;
        return 0;
    }

    // Создаём программу для видеокарты - совокупность двух шейдеров.
    static GLint makeShader(const char* vtxSource, const char* frgSource)
    {
        // Говорим OpenGL, что хотим создать новую программу для видеокарты
        GLint program = glCreateProgram();
        // и генерируем шейдеры, которые в неё войдут.
        GLuint vtxShader = compileShader(vtxSource, GL_VERTEX_SHADER);
        GLuint frgShader = compileShader(frgSource, GL_FRAGMENT_SHADER);
        // Если хотя бы один из шейдеров не скомпилировался - всё плохо,
        // говорим, что программу сделать тоже нельзя.
        if (vtxShader == 0 || frgShader == 0)
        {
            glDeleteShader(vtxShader);
            glDeleteShader(frgShader);
            glDeleteProgram(program);
            Log(LOG_ERROR) << "Could not compile some shaders";
            return -1;
        }
        // Подключаем шейдеры к нашей программе
        glAttachShader(program, vtxShader);
        glAttachShader(program, frgShader);
        // и помечаем их как более не используемые (они будут храниться в нашей программе,
        // за её пределами они не нужны).
        glDeleteShader(vtxShader);
        glDeleteShader(frgShader);
        // Связываем шейдеры: входы фрагментного шейдера должны соответствовать выходам вершинного.
        glLinkProgram(program);
        // Проверяем, удалось ли нам связать шейдеры
        GLint linkStatus;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus == GL_TRUE)
        {
            // Всё получилось; запрашиваем нахождение переменных для входных данных в программе
            // и говорим, какой номер будет у нашей программы для видеокарты.
            /*glState.loc.pos = glGetAttribLocation(program, "pos");
            glState.loc.color = glGetAttribLocation(program, "v_color");
            glState.loc.uProj = glGetUniformLocation(program, "proj");
            glState.loc.uTransform = glGetUniformLocation(program, "transform");
            Log(LOG_DEBUG) << "Position location: " << glState.loc.pos;
            Log(LOG_DEBUG) << "Color location: " << glState.loc.color;*/
            return program;
        }
        // Что-то пошло не так: пытаемся выяснить, что именно.
        GLint logLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        // Выделяем достаточно памяти для хранения сообщений об ошибке
        // и копируем это сообщение туда.
        char *log = new char[logLength];
        glGetProgramInfoLog(program, logLength, nullptr, log);
        // Указываем пользователю, почему связывание провалилось, и освобождаем память
        // для сообщения.
        Log(LOG_ERROR) << "Could not compile shader: " << log;
        delete log;
        return -1;
    }

    static GLint currentProgram;

    void introspect()
    {
        const int bufSize = 256;
        GLint size;
        GLenum type;
        char buf[bufSize];
        GLint numVars;
        GLint numUniforms;
        glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &numVars);
        glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &numUniforms);
        Log(LOG_DEBUG) << "Attribute count: " << numVars << ", uniform count: " << numUniforms;
        for(GLuint i = 0; i < numVars; ++i)
        {
            glGetActiveAttrib(program, i, bufSize, NULL, &size, &type, buf);
            GLint loc = glGetAttribLocation(program, buf);
            std::string name = buf;
            vars.push_back(name);
            varLoc[name] = loc;
            Log(LOG_DEBUG) << "[Attribute " << i
                           <<  "] name: " << name
                           << ", location: " << loc
                           << ", size: " << size
                           << ", type: " << type;
        }
        for(GLuint i = 0; i < numUniforms; ++i)
        {
            glGetActiveUniform(program, i, bufSize, NULL, &size, &type, buf);
            GLint loc = glGetUniformLocation(program, buf);
            std::string name = buf;
            unifs.push_back(name);
            unifLoc[name] = loc;
            Log(LOG_DEBUG) << "[Uniform " << i
                           <<  "] name: " << name
                           << ", location: " << loc
                           << ", size: " << size
                           << ", type: " << type;
        }
    }
    GLint program;

    std::unordered_map<std::string, GLuint> varLoc;
    std::unordered_map<std::string, GLuint> unifLoc;
    std::vector<std::string> vars;
    std::vector<std::string> unifs;

public:
    Shader(const char* vtxSource, const char* frgSource)
    {
        program = makeShader(vtxSource, frgSource);
        if (program >= 0)
        {
            Log(LOG_DEBUG) << "Created shader program " << program;
            introspect();
        }
        else
        {
            Log(LOG_ERROR) << "Could not create shader from source";
            Log(LOG_ERROR) << "---\nVertex shader source:\n\n" << vtxSource;
            Log(LOG_ERROR) << "---\nFragment shader source:\n\n" << frgSource;
        }
    }

    ~Shader()
    {
        Log(LOG_DEBUG) << "Removing shader program " << program;
        glDeleteProgram(program);
    }

    void use()
    {
        if (program < 0)
        {
            Log(LOG_ERROR) << "Attempt to use invalid shader";
            return;
        }
        if (program != currentProgram)
        {
            glUseProgram((GLuint)program);
        }
    }

    GLint getAttr(const std::string& attrName) const
    {
        auto varIter = varLoc.find(attrName);
        if (varIter != varLoc.end())
        {
            return varIter->second;
        }
        return -1;
    }

    GLint getUnif(const std::string& unifName) const
    {
        auto unifIter = unifLoc.find(unifName);
        if (unifIter != varLoc.end())
        {
            return unifIter->second;
        }
        return -1;
    }

    const std::vector<std::string>& listAttrs() const
    {
        return vars;
    }

    const std::vector<std::string>& listUnifs() const
    {
        return unifs;
    }
};

#endif //VINOKUROVA_STRESSCUBE_SHADER_H
